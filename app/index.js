var Base = require('../shared/base');
const path = require('path');

module.exports = Base.extend({
  initializing: function () {
    this.config.set("services", this.config.get('services') || []);
  },

  prompting: function() {
    return this.prompt([{
      type    : 'input',
      name    : 'organization',
      message : 'The name of your organization',
      default : this.config.get("organization")
    },{
      type    : 'input',
      name    : 'application',
      message : 'The name of this application',
      default : this.config.get("application")
    },{
      type    : 'input',
      name    : 'author',
      message : 'your name',
      default : this.config.get("author")
    }]).then((answers) => {
      this.config.set("organization", answers.organization);
      this.config.set("application", answers.application);
      this.config.set("author", answers.author);
    });
  },

  default: function(){
    this.options = {
      organization: this.config.get("organization"),
      author: this.config.get("author"),
      application: this.config.get("application"),
      services: this.config.get('services')
    };

    this.composeWith(require.resolve('./serverless/index'), this.options);
  }
});
