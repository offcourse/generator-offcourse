const Base = require('../../shared/base');
const path = require('path');
const axios = require('axios');

function fetch(url){
  return axios.get(url);
}

let buildScript = `
curl "https://s3.amazonaws.com/offcourse-services-info-global/development.json" > ./infrastructure.json &&
if [ $CI_SERVER ]
then
npm install &&
boot build
else echo "running local, skipping build"
fi`;

module.exports = Base.extend({
  initializing: function () {
    var done = this.async();
    var self = this;
    var servicesInfo  = {endpoint: "https://s3.amazonaws.com/offcourse-services-info-global"};
    this.config.set("services_info", servicesInfo);
    fetch(`${servicesInfo.endpoint}/development.json`)
      .then(function (response) {
        self.service_roles = response.data.service_roles;
        self.infrastructure = response.data;
        done();
      });
  },

  writing: function() {
    let name = `${this.config.get("organization")}-${this.config.get("application")}`;
    let project = {
      name: name,
      description: `${name} by ${this.config.get("author")}`,
      role: this.service_roles.default,
      memory: 128,
      timeout: 20,
      hooks: {
        "build": buildScript
      }
    };
    this.fs.writeJSON("project.json", project);

    let destinationPath = path.join("infrastructure", "base.json");
    this.fs.writeJSON(destinationPath, this.infrastructure);
  }
});
