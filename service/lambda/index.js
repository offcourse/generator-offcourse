const Base = require('../../shared/base.js');
const _ = require('lodash');
const path = require('path');

module.exports = Base.extend({
  constructor: function () {
    Base.apply(this, arguments);
  },

  writing: function() {
    let environment = {};
    let overrides = {
      invokeLocal: "./node_modules/node-lambda/bin/node-lambda run -H index.handle -j",
      watch: "boot dev",
      build: "boot build"
    };

    let destinationPath = path.join("functions", this.options.service, "function.json");
    this.fs.writeJSON(destinationPath, { environment });
    this._copyTemplates(["package.json", "index.js" , "context.json", "event.json"],
                        _.merge(this.options, overrides));
  },

  install: function(){
    let deps = this.options.dependencies;
    let oldDir = process.cwd();
    var newdir = path.join(oldDir, "functions", this.options.service);
    this.yarnInstall(deps, {'exact': true}, ()=> {}, {cwd: newdir});
  }
});
