const Base = require('../shared/base');
const _ = require('lodash');
const Immutable = require('immutable');
const axios = require('axios');

function fetch(base, datapoint){
  return axios.get(`${base}/${datapoint}.json`);
}

let toDeps = (dependencies) => _
    .chain(dependencies)
    .mapKeys((v, k) => `${k}@${v}`)
    .keys()
    .value();

module.exports = Base.extend({

  _registerService(){
    let oldNames = Immutable.Set(this.config.get('services'));
    let newNames = oldNames.add(this.options.service);
    this.config.set('services', newNames.toJS());
  },

  _removeService(){
    let oldNames = Immutable.Set(this.config.get('services'));
    let newNames = oldNames.remove(this.options.service);
    this.config.set('services', newNames.toJS());
    this.fs.delete(this.options.service);
  },

  constructor: function () {
    Base.apply(this, arguments);
    this.argument('serviceName', { type: String, required: true});
    this.option('remove');
  },

  initializing() {
    let options = {
      organization: this.config.get("organization"),
      author: this.config.get("author"),
      service: this.options.serviceName,
      remove: this.options.remove
    };

    var done = this.async();
    var self = this;
    var endpoint  = this.config.get("services_info").endpoint;
    let datapoints = ["development/infrastructure", "services-shared", "shared"];
    let requests = _.map( datapoints, (datapoint) => fetch(endpoint, datapoint));
    axios.all(requests)
      .then(axios.spread(function(evi, ssi, si) {
        options.dependencies = toDeps(ssi.data.dependencies);
        options.servicesSharedVersion = ssi.data.version;
        options.sharedVersion = si.data.version;
        options.infrastructure = evi.data;
        self.options = options;
        if(!self.options.remove){
          self.composeWith(require.resolve('./lambda/index'), self.options);
          self.composeWith(require.resolve('./cljs-app/index'), self.options);
          done();
        }
      }));
  },

  prompting: function() {
    let questions = [];

    if(this.options.remove){
      questions.push({
        type    : 'confirm',
        name    : 'confirmRemove',
        message : `Do you really want to remove the ${this.options.service} service?`
      });
    }
    return this.prompt(questions).then((answers) => {
      this.options.remove = answers.confirmRemove;
    });
  },

  configuring(){
    if(this.options.remove){
      this._removeService();
    } else {
      this._registerService();
    }
  },


  install: function(){
    let deps = [];
    if(!_.isEmpty(deps)){
      this.yarnInstall(deps, {'exact': true});
    }
  }
});
