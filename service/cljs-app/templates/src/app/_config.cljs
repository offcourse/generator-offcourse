(ns <%= service %>.config
    (:require [cljs.nodejs :as node]
              [shared.protocols.convertible :as cv]))

(def infrastructure (cv/to-clj (node/require "../infrastructure.json")))

(def adapters {})
