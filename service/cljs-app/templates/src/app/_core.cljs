(ns <%= service %>.core
    (:require [backend-shared.service.index :as service]
              [<%= service %>.config :as config]
              [cljs.core.async :as async :refer [<!]]
              [shared.protocols.actionable :as ac]
              [shared.protocols.convertible :as cv]
              [shared.protocols.eventful :as ev])
  (:require-macros [cljs.core.async.macros :refer [go]]))

(defn ^:export handler [& args]
  (go
    (let [{:keys [event] :as service} (apply service/create config/adapters args)]
      (ev/respond service [:reply event]))))

(defn -main [] identity)
(set! *main-cli-fn* -main)
